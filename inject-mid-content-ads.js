export default function injectMidContentAds(article) {
  const cheerio = require('cheerio');
  const $ = cheerio.load(article.html);
  let blacklist = article.blacklist;
  let tempHTML = '<article>';
  let copyCount = 0,
    imgCount = 0,
    adCount = 0,
    pgphCount = 0
    ;
  let imgTest = function(el) {
    if ($(el).is('img')) {
      return true;
    }
  }
  let pgphTest = function(el) {
    if ($(el).is('p')) {
      return true;
    }
  }
  if (blacklist.length) return false
  let pgphTotLength = $('article p').length;
  let $ad = $('<div>', { 'class': 'ad', 'text': 'ad'})


  $('article > *').each(function(i, value) {
    if (value !== '') {
      // imgTest(value) ? console.log('img', $(value).attr('src')) : console.log('text', $(value).text())
      if (imgTest(value)) {
        tempHTML += '<img src="' + $(value).attr('src') + '">'
        imgCount += 1
      } else {
        copyCount += $(value).text().split(' ').length
        tempHTML += "<p>" + $(value).text() + "</p>"
        pgphCount += 1
        if (i % 4 == 3 && i !== pgphTotLength - 1) {
          if (imgTest(value)) {
            tempHTML += '<div class="ad">ad</div>'
            adCount += 1
          } else if (!imgTest(value)) {
            $(value).after($ad)
            tempHTML += '<div class="ad">ad</div>'
            adCount += 1
          }
        }
      }
    }
  })

  tempHTML += '</article>';

  // console.log('pgphCount', pgphCount)
  // console.log('pgphTotLength', pgphTotLength)
  // console.log('imgCount', imgCount)
  // console.log('pgphTotLength', pgphTotLength)
  // console.log('copyCount', copyCount)
  // console.log('adCount', adCount)
  // console.log('tempHTML', tempHTML)

  article.html = tempHTML
  return article.html;
}
